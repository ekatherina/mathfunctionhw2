//
//  ViewController.swift
//  HW 2
//
//  Created by Katherina🌹 on 3/4/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        numberCompassion( numberA: 12, numberB: 10)
        squareAndCubeNumber(number: 5)
        backAndForth(number: 6)
        countingCommonDividers(number: 5)
        perfectNumber(number: 5)
    }
    //сравнение чисел
    func numberCompassion ( numberA : Int, numberB : Int) {
        if numberA > numberB {
            print("Число \(numberA) больше числа \(numberB)")
        }
        else {
            print("Число \(numberB) больше числа \(numberA)")
        }
    }

    //квадрат и куб цисла
    func squareAndCubeNumber ( number : Int) {
        let square = number * number
        let cube   = number * number * number
        print("Куб \(number) = \(square) \nКвадрат \(number) = \(cube) ")
    }
    //Вывести на экран все числа до заданного и в обратном порядке до 0
    func backAndForth (number : Int) {
        let a = 0
        var numberA = number
        for a in 0...number {
            print ("\(a) \(numberA - a)")
        }
    }
    
    //Подсчитать общее число делителей числа и вывести их
    func countingCommonDividers (number : Int) {
        let A = number
        let B = 1
        var count = 0
        for B in B...A {
            if A%B == 0 {
                print ("divider :\(B)")
                count += 1
            }
        }
        print ("Count divider of \(A) : \(count) ")
    }
    
    //Проверить, является ли заданное число совершенным и найти их
    func perfectNumber (number: Int) {
        let P = number
        let D = 1
        var sum = 0
        for D in D..<P {
            if P%D == 0 {
                sum += D
                print ("divider \(P) = \(D)")
            }
            else {
                sum += 0
            }
        }
        if sum == P {
            print ("Number \(P) perfect , summ of dividers = \(sum)")
        }
        else {
            print ("Number \(P) not perfect  ")
        }
    }
}
